type t('data);

module Private = {
  module Make = {
    [@bs.send]
    external withOptions: (string, NotificationOptions.t('data))
      => t('data)
      = "make";

    [@bs.send] external withoutOptions: (string) => t('data) = "make";
  };

  [@bs.get]
  external actions: t('data) => Js.Nullable.t(list(NotificationAction.t))
    = "actions";

  [@bs.get]
  external vibrate: t('data) => Js.Nullable.t(list(int)) = "vibrate";
};

let make = (~options=?, ~title: string): t('data) => {
  switch (options) {
  | None => Private.Make.withoutOptions(title)
  | Some(o) => Private.Make.withOptions(title, o)
  };
};

/* static properties */

[@bs.get] external permission: unit => NotificationPermission.t = "permission";

[@bs.get] external maxActions: unit => int = "maxActions";

/* instance properties */
let actions = (~t: t('data)): list(NotificationAction.t) => {
  switch(Js.Nullable.toOption(Private.actions(t))) {
  | None => []
  | Some(xs) => xs
  }
};
[@bs.get] external badge: t('data) => string = "badge";
[@bs.get] external body: t('data) => string = "body";
[@bs.get] external data: t('data) => option('data) = "data";
[@bs.get] external dir: t('data) => NotificationOptions.dir = "dir";
[@bs.get] external lang: t('data) => string = "lang";
[@bs.get] external tag: t('data) => string = "tag";
[@bs.get] external icon: t('data) => string = "icon";
[@bs.get] external image: t('data) => string = "image";
[@bs.get] external renotify: t('data) => bool = "renotify";
[@bs.get] external requireInteraction: t('data)=> bool = "requireInteraction";
[@bs.get] external silent: t('data) => bool = "silent";
/* TODO timestamp. Those are integers, right? */
[@bs.get] external title: t('data) => string = "title";


let vibrate = (~t: t('data)): list(int) => {
  switch(Private.vibrate(t) -> Js.Nullable.toOption) {
  | None => []
  | Some(xs) => xs
  }
}

/* handlers */

type mouseHandler = Dom.mouseEvent => unit;
[@bs.get] external get_onclick: t('data) => mouseHandler = "onclick";
[@bs.set] external set_onclick: (t('data), mouseHandler) => unit = "onclick";

type handler = Dom.event => unit;
[@bs.get] external get_onclose: t('data) => handler = "onclose";
[@bs.set] external set_onclose: (t('data), handler) => unit = "onclose";

[@bs.get] external get_onerror: t('data) => handler = "onerror";
[@bs.set] external set_onerror: (t('data), handler) => unit = "onerror";

[@bs.get] external get_onshow: t('data) => handler = "onshow";
[@bs.set] external set_onshow: (t('data), handler) => unit = "onshow";

type handler_like('a) = 'a => unit; //TODO this could be more specific
[@bs.send] external addEventListener: (t('data), string, handler_like('a))
  => unit
  = "addEventListener";

/* static methods */
[@bs.send]
external requestPermission: unit => Js.Promise.t(NotificationPermission.t)
  = "requestPermission";

/* instance methods */
[@bs.send] external close: t('data) => unit = "close";