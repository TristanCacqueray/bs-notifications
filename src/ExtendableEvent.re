type _t('a);
type t_like('a) = Dom.event_like(_t('a));
type t = t_like(Dom._baseClass);

[@bs.send] external waitUntil: (t_like('subtype), Js.Promise.t('a)) => unit
  = "waitUntil";


