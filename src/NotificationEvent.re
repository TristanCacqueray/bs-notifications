type _notificationEvent('a);
type notificationEvent_like('a) = ExtendableEvent_like(_notificationEvent('a));
type t('data) = notificationEvent_like(Dom._baseClass);


module Private = {
  module Make = {
    [@bs.send]
    external withOptions: (string, NotificationEventInit.t('data))
      => t('data)
      = "make";

    [@bs.send] external withoutOptions: (string) => t('data) = "make";
  };
};


let make = (~options=?, ~type_: string): t('data) => {
  switch (options) {
  | None => Private.Make.withoutOptions(type_)
  | Some(o) => Private.Make.withOptions(type_, o)
  };
};

[@bs.get] external notification: t('data) => Notification.t('data)
  = "notification";
[@bs.get] external action: t('data) => string = "action";