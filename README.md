# bs-notifications

This package closely wraps the web
[Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API).
It will not include the Notification API Service Worker Additions. Instead,
it will be a dependency of the forthcoming `bs-service-worker`. Other than 
that, the whole API is here, but untested as yet. 

## Installation
`npm install bs-notifications`

## Implemented
- [X] [ExtendableEvent](https://developer.mozilla.org/en-US/docs/Web/API/ExtendableEvent)
- [X] [Notification](https://developer.mozilla.org/en-US/docs/Web/API/Notification)
- [X] [NotificationAction](https://developer.mozilla.org/en-US/docs/Web/API/Notification/actions)
- [X] [NotificationEvent](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent)
- [X] [NotificationEventInit](https://developer.mozilla.org/en-US/docs/Web/API/NotificationEvent/NotificationEvent)
- [X] [NotificationOptions](https://developer.mozilla.org/en-US/docs/Web/API/Notification/Notification)
- [X] [NotificationPermission](https://developer.mozilla.org/en-US/docs/Web/API/Notification/permission)


## Notes
### 'data
You'll notice these types often have a generic type parameter `'data`. This
represents the `data` property of the NotificationOptions passed into the 
Notification constructor. It can be any structured clonable type, though, not
any type. 

### ExtendableEvent
This interface is technically considered part of the Service Worker API, but 
I've included it here instead to break the circular dependency between 
Service Worker API and Notifications API. In my small universe, 
bs-service-worker will depend on bs-notifications, one way. 

It would be cool if I could do this with implementation inheritance instead,
but I'm struggling to make that work with the type parameter. Feel free to 
advise. 

### Notification
I decided for the moment, not to safely wrap every property as options, even 
though some
of them don't enjoy wide support. That would be a good feature to add for
later. For now, please refer to the documentation of your supported browsers 
to determine what you can use.

## Examples
Forthcoming, my first next priority will be using this package in 
`bs-service-worker`. I'll post links and snippets here afterwards. 

## Contributing
I'm a beginner. I'd be surprised if all of this were definitely right. 
[@ me](https://twitter.com/webbureaucrat) or send me a PR if you find 
anything you think I
should take a second look at. (Stylistically, though, I'll warn you I'm pretty
dug in, at least for now)

I'd be very happy if anyone wants to write tests. If you want to write an 
example, or link to your real project that uses this library, I'd also be 
very happy to add a link to it here. 

Also, if anyone wants to go into Notification.re and safely wrap each `init`
property Firefox doesn't support with an empty string fallback, I'd really
appreciate it. Look at `vibrate` if you need an example. 


## on the ReScript Rebrand

I'm ignoring it until things have settled down just a little. After that, I'm
thinking I'll fork the project and maintain two versions, just in case some
people aren't as willing as I am to upgrade. 

## For further reading
I *strongly* recommend you check out my 
[catch-all documentation](https://webbureaucrat.gitlab.io/posts/issues-and-contribution/)
on my 
projects. It describes how to get in touch with me if you have any questions, 
how to contribute code, coordinated disclosure of security vulnerabilities, 
and more. It will be regularly updated with any information I deem relevant
to my side projects. 

Most of all, feel free to get in touch! I'm delighted to hear suggestions
or field questions. 
